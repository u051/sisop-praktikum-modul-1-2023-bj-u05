#!/bin/bash
image="https://a.travel-assets.com/findyours-php/viewfinder/images/res70/210000/210083-Central-Java.jpg"

download_image() {
	hour=$(date +'%H')
	hour=$((10#$hour))
	i=1
	
	while [ -e Kumpulan_$i ]
	do
		((i++))
	done
	mkdir "Kumpulan_$i"
	
	if [ $hour -eq 0 ] ; then
		#echo "downloading..."
		wget -P Kumpulan_$i "$image" -O Kumpulan_1/perjalanan_1.jpg
	else
		for j in $(seq 1 $hour)
		do
		#echo "downloading..."
		wget -P Kumpulan_$i "$image" -O Kumpulan_$i/perjalanan_$j.jpg
		done
	fi
}

zip_folders(){
	#echo "zipping..."
	count=1
	
	while [ -f "Devil_$count.zip" ]
	do
		((count++))
	done
	zip -r Devil_$count.zip Kumpulan_*
	#echo $count
	
	count2=1
	while [ -e Kumpulan_$count2 ]
	do
		rm -r Kumpulan_$count2
		((count2++))
	done
}

if [ $1 == "zip" ]
then
	zip_folders
else
	download_image
fi

#crontab setup:
#
#0 */10 * * * /sisop-praktikum-modul-modul-1-2023-bj-u05/soal2/kobeni_liburan.sh
#0 0 * * * /sisop-praktikum-modul-modul-1-2023-bj-u05/soal2/kobeni_liburan.sh zip
#
#the path to the script can be changed accordingly.
