#!/bin/bash
echo "Top 5 Uni in Japan"
awk -F ',' '/Japan/ {print $2' 2023_QS_World_University_Rankings.csv|head -n 5

echo "nomor 2"
awk -F ',' '/Japan/ {print $2' 2023_QS_World_University_Rankings.csv|sort -n|head -n 5

echo "nomor 3"
awk -F ',' '/Japan/ {print $2' 2023_QS_World_University_Rankings.csv|sort -k 2 -n|tail -n 10

echo "Masukan Kata Kunci"
read key
case "$key" in
"keren")
awk -F ',' '/Keren/ {print $2}' 2023_QS_World_University_Rankings.csv
;;
esac
