# sisop-praktikum-modul-modul 1-2023-BJ-U05

## Group U05

Abiansyah Adzani Gymnastiar (5025211077)
I Putu Arya Prawira Wiwekananda (5025211065)
Satria Surya Prana (5025211073)

## Soal 1
1. Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi  : 

- Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan        ranking tertinggi di Jepang.
- Karena Bocchi kurang percaya diri dengan kemampuannya, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang. 
- Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.
- Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.

### Pengerjaan dan Pembahasan
- Dikerjakan oleh   : Satria Surya Prana (5025211073)
- Code untuk poin 1 (Menampilkan top 5 Universitas di Jepang)
    ```bash
    echo "Top 5 Uni in Japan"
    awk -F ',' '/Japan/ {print $2' 2023_QS_World_University_Rankings.csv|head -n 5
    ```
    echo dibuat untuk memberikan output dan pembatas saat file university_survey.sh di bash dan awk -F memisahkan file dengan koma(untuk csv) selanjutnya /Japan/ untuk mengambil kata kunci yang spesifik dalam kasus poin pertama adalah kata "Japan" lalu print $2 untuk menampilkan nama universitas yang dicari dan head -n 5 untuk menampilkan 5 teratas dari hasil yang ditemukan
- Code Untuk Poin 2 (Menampilkan top 5 Universitas dengan fsr score terendah di Jepang)
    ```bash
    echo "nomor 2"
    awk -F ',' '/Japan/ {print $2' 2023_QS_World_University_Rankings.csv|sort -n|head -n 5
    ```
    echo kembali dibuat untuk memberikan output kali ini untuk menunjukan bahwa ini adalah jawaban poin kedua, awk kembali digunakan namun disini kami mengambil kolom ke 9 juga yang merupakan kolom fsr score, setelah itu kami sorting dengan sort -n dimana -n digunakan untuk sorting numerik, sort disini sudah dalam bentuk ascending(dari terkecil), dan head -n 5 untuk menampilkan 5 hasil teratas.
- Code Untuk Poin 3 (Menampilkan top 10 Universiras dengan ger rank tertinggi di Jepang)
    ```bash
    echo "nomor 3"
    awk -F ',' '/Japan/ {print $2' 2023_QS_World_University_Rankings.csv|sort -k 2 -n|tail -n 10
    ```
    echo kembali dibuat untuk memberikan output kali ini untuk menunjukan bahwa ini adalah jawaban poin ketiga, lalu awk disini digunakan untuk mencari kata kunci Jepang dari kolom 20 dan 2 dengan (:) sebagai pemisahnya, selanjutnya kembali kita lakukan sorting numerik dengan kolom kedua sebagai acuannya, lalu tail -n 10 digunakan untuk mengambil 10 hasil terbawah.
- Code Untuk Poin 4 (Menampilkan Universitas dengan kata kunci "keren")
    ```bash
    echo "Masukan Kata Kunci"
    read key
    case "$key" in
    "keren")
    awk -F ',' '/Keren/ {print $2}' 2023_QS_World_University_Rankings.csv
    ;;
    esac
    ```
     echo kembali dibuat untuk memberikan output kali ini untuk memberitahu user untuk memasukan kata kunci lalu ada read key yang digunakan untuk menerima input saat file university_survey.sh di bash lalu ada branching dimana jika input yang diterima adalah "keren" maka akan ditampilkan universitas yang memiliki kata kunci "keren".
## Soal 2
2. Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut.

- Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:
     - File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
     - File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst)
- Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas.

### Pengerjaan dan Pembahasan

- Dikerjakan oleh   : Abiansyah Adzani Gymnastiar (5025211077)
- Code untuk poin 1 (Download gambar)
    - download_image function :
        ```bash
        download_image() {
	    hour=$(date +'%H')
	    hour=$((10#$hour))
	    i=1

	    while [ -e Kumpulan_$i ]
	    do
		((i++))
	    done
	    mkdir "Kumpulan_$i"

	    if [ $hour -eq 0 ] ; then
            #echo "downloading..."
		    wget -P Kumpulan_$i "$image" -O Kumpulan_1/perjalanan_1.jpg
	    else
		    for j in $(seq 1 $hour)
		    do
		    #echo "downloading..."
		    wget -P Kumpulan_$i "$image" -O Kumpulan_$i/perjalanan_$j.jpg
		    done
	    fi
        }
        ```
        fungsi ini dapat berjalan dengan membuat variabel hour sebagai integer jam saat ini. Lalu dilanjutkan dengan while loop untuk mengecek Kumpulan_i keberepa yang akan dibuat (misalkan sudah ada kumpulan_1 sampai kumpulan_3, makan akan dibuat kumpulan_4). Folder Kumpulan baru akan dibuat dengan mkdir "Kumpulan_$i". Setelah itu, if else akan dipakai untuk mendownload gambar perjalanan_i.jpg yang dilakukan sebanyak $hour. jika hour = 0, maka akan didownload 1 kali. File gambarnya akan langsung didownload di dalam folder Kumpulan_i.
    
    - zip_folders function :
        ```bash
        zip_folders(){
	    #echo "zipping..."
	    count=1

	    while [ -f "Devil_$count.zip" ]
	    do
	    	((count++))
	    done
	    zip -r Devil_$count.zip Kumpulan_*
	    #echo $count

	    count2=1
	    while [ -e Kumpulan_$count2 ]
	    do
	    	rm -r Kumpulan_$count2
	    	((count2++))
	    done
        }
        ```
        Fungsi ini dapat dapat berjalan dengan menggukanan while loop untuk menentukan file Devil_i.zip keberapa yang akan dibuat. zipnya sendiri dibuat dengan zip -r Devil_$count.zip Kumpulan_*, dimana Kumpulan_* merupakan isi dari zip yang akan dibuat tersebut (Kumpulan_* adalah seluruh Kumpulan_n yang ada dalam direktori). Lalu akan digunakan while loop kedua, yang berfungsi untuk menghapus seluruh Kumpulan_n yang ada di dalam direktori (setelah dimasukkan kedalam zip).
    - main function :
        ```bash
        if [ $1 == "zip" ]
        then
	        zip_folders
        else
	        download_image
        fi
        ```
        Disini, kita dapat menentukan function apa yang akan dijalankan oleh script dengan memasukan argumen $1 yang sesuai, dimana jika tidak memasukan argumen maka akan menjalankan download_image function, sedangkan dengan memasukan argumen "zip" maka akan menjalankan zip_folders function.
    
    - crontab setup :
        ```
        0 */10 * * * /sisop-praktikum-modul-modul-1-2023-bj-u05/soal2/kobeni_liburan.sh
        ```
        untuk menjalankan script dengan fungsi mendownload gambar sekali setiap 10jam
        ```
        0 0 * * * /sisop-praktikum-modul-modul-1-2023-bj-u05/soal2/kobeni_liburan.sh zip
        ```
        untuk menjalankan script dengan fungsi zip folder sekali setiap hari (pada jam 00:00)
- Screenshot program :
    ![judul](https://gitlab.com/u051/sisop-praktikum-modul-1-2023-bj-u05/-/raw/main/images/Screenshot_from_2023-03-10_20-41-07.png)
    ![judul](https://gitlab.com/u051/sisop-praktikum-modul-1-2023-bj-u05/-/raw/main/images/Screenshot_from_2023-03-10_20-47-52.png)
    ![judul](https://gitlab.com/u051/sisop-praktikum-modul-1-2023-bj-u05/-/raw/main/images/Screenshot_from_2023-03-10_20-48-28.png)
- Demo dan revisi
    - Demo berjalan dengan baik dan menurut penguji tidak diperlukan revisi untuk soal2

## Soal3
3. Peter Griffin hendak membuat suatu sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin juga membuat sistem login yang dibuat di script retep.sh
- Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut :
     - Minimal 8 karakter
     - Memiliki minimal 1 huruf kapital dan 1 huruf kecil
     - Alphanumeric
     - Tidak boleh sama dengan username 
     - Tidak boleh menggunakan kata chicken atau ernie

- Setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.
     - Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
     - Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
     - Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
     - Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in

### Pengerjaan dan Pembahasan

- Dikerjakan oleh   : I Putu Arya Prawira Wiwekananda (5025211065)
- Code untuk louis.bash
     - Register Fuction :
     ```bash


     function is_password_valid {
  
     if [ ${#1} -lt 8 ]; then
        return 1
     fi

     if ! echo "$1" | grep -q '[[:upper:]]' || ! echo "$1" | grep -q '[[:lower:]]'; then
     return 1
     fi

     if ! echo "$1" | grep -q '[[:alnum:]]'; then
     return 1
     fi

     if [ "$1" == "$username" ] || echo "$1" | grep -qE 'chicken|ernie'; then
     return 1
     fi

     return 0
     }

     read -p "Enter username: " username
     read -s -p "Enter password: " password
     echo


     if ! is_password_valid "$password"; then
     echo "Invalid password!"
     exit 1
     fi


     if grep -q "^$username:" /users/users.txt; then
     echo "User already exists!"
     echo "$(date +'%Y/%m/%d %H:%M:%S') REGISTER: ERROR User $username already exists" >> /users/log.txt
     exit 1
     fi

     echo "$username:$password" >> /users/users.txt
     echo "User registered successfully!"
     echo "$(date +'%Y/%m/%d %H:%M:%S') REGISTER: INFO User $username registered successfully" >> /users/log.txt
     exit 0
     ```
- Code untuk retep.sh
     - Login Function :
     ```bash


     read -p "Enter username: " username
     read -s -p "Enter password: " password
     echo


     if ! grep -q "^$username:" /users/users.txt; then
     echo "Invalid username or password!"
     echo "$(date +'%Y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> /users/log.txt
     exit 1
     fi

     correct_password=$(grep "^$username:" /users/users.txt | cut -d ":" -f 2)

     if [ "$password" != "$correct_password" ]; then
     echo "Invalid username or password!"
     echo "$(date +'%Y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> /users/log.txt
     exit 1
     fi

     echo "Login successful!"
     echo "$(date +'%Y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in" >> /users/log.txt
     exit 0
     ```
     
## Soal4

Soal tidak selesai karena waktu yang sudah mendekati deadline
