#!/bin/bash

# function to check if a password is valid
function is_password_valid {
  # check length
  if [ ${#1} -lt 8 ]; then
    return 1
  fi

  # check for uppercase and lowercase letters
  if ! echo "$1" | grep -q '[[:upper:]]' || ! echo "$1" | grep -q '[[:lower:]]'; then
    return 1
  fi

  # check for alphanumeric characters
  if ! echo "$1" | grep -q '[[:alnum:]]'; then
    return 1
  fi

  # check if the password is the same as the username or contains "chicken" or "ernie"
  if [ "$1" == "$username" ] || echo "$1" | grep -qE 'chicken|ernie'; then
    return 1
  fi

  # password is valid
  return 0
}

# get username and password from user input
read -p "Enter username: " username
read -s -p "Enter password: " password
echo

# check if password is valid
if ! is_password_valid "$password"; then
  echo "Invalid password!"
  exit 1
fi

# check if user already exists
if grep -q "^$username:" /users/users.txt; then
  echo "User already exists!"
  echo "$(date +'%Y/%m/%d %H:%M:%S') REGISTER: ERROR User $username already exists" >> /users/log.txt
  exit 1
fi

# add user to users.txt file
echo "$username:$password" >> /users/users.txt
echo "User registered successfully!"
echo "$(date +'%Y/%m/%d %H:%M:%S') REGISTER: INFO User $username registered successfully" >> /users/log.txt
exit 0
