#!/bin/bash

# get username and password from user input
read -p "Enter username: " username
read -s -p "Enter password: " password
echo

# check if user exists in users.txt file
if ! grep -q "^$username:" /users/users.txt; then
  echo "Invalid username or password!"
  echo "$(date +'%Y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> /users/log.txt
  exit 1
fi

# get the password for the user from users.txt file
correct_password=$(grep "^$username:" /users/users.txt | cut -d ":" -f 2)

# check if password is correct
if [ "$password" != "$correct_password" ]; then
  echo "Invalid username or password!"
  echo "$(date +'%Y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> /users/log.txt
  exit 1
fi

# login successful
echo "Login successful!"
echo "$(date +'%Y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in" >> /users/log.txt
exit 0